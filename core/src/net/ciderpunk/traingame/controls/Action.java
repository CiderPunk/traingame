package net.ciderpunk.traingame.controls;

import com.badlogic.gdx.utils.Array;

/**
 * Created by matthewlander on 13/10/15.
 */
public abstract class Action {
	public final String name;
	public final String key;
	public final int defKeyCode;
	protected boolean active;
	protected final Array<IActionConsumer> consumers;

	public void addConsumer(IActionConsumer consumer){
		this.consumers.add( consumer);
	}

	public void removeConsumer(IActionConsumer consumer){
		this.consumers.removeValue(consumer, true);
	}

	public Action(String name, String key, int defaultKeyCode) {
		this.key = key;
		this.name = name;
		this.defKeyCode = defaultKeyCode;
		this.consumers = new Array<IActionConsumer>();
	}

	public boolean keyDown() {
		//Gdx.app.log("input",name + " pressed\n");
		boolean result = false;
		for (IActionConsumer consumer : consumers) {
			result = consumer.startAction(this) ? true : result;
		}
		return result;
	}

	public boolean keyUp() {
		//Gdx.app.log("input", name + " released\n");
		boolean result = false;
		for (IActionConsumer consumer : consumers) {
			result = consumer.endAction(this) ? true : result;
		}
		return result;
	}

}

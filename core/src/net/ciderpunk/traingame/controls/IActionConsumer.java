package net.ciderpunk.traingame.controls;

/**
 * Created by matthewlander on 13/10/15.
 */
public interface IActionConsumer {

	public boolean startAction(Action action);
	public boolean endAction(Action action);
}

package net.ciderpunk.traingame.map;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.BaseLight;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.utils.Disableable;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Json;
import net.ciderpunk.traingame.Constants;


/**
 * Created by Matthew on 02/05/2016.
 */
public class Map  implements Disposable{

	IntMap<Chunk> ChunkMap;
	Model model;
  private final Environment env = new Environment();
  MapDef def;

	final Vector2 ChunkBounds = new Vector2():

	public Map(FileHandle file) throws Exception {
		env.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.3f, 0.3f, 1.0f));
		Json json = new Json();
		def = json.fromJson(MapDef.class, file);
		//check map data is valid
		int squares = def.width * def.height;
		if (def.depths.length != ((def.width + 1) * (def.height + 1))){
			throw new Exception("missing depth data");
		}
		if (def.getTileData().length() != squares){
			throw new Exception("missing tile data");
		}

		ChunkBounds.set( MathUtils.ceil(def.width / Constants.ChunkSize),MathUtils.ceil(def.height / Constants.ChunkSize));
	}


	public Chunk getChunk(int x,int y){
	Chunk chunk =null;
		if (x >= 0 && y >= 0 &&
		ChunkBounds.x > x && ChunkBounds.y > y) {
			int key = x + (y >> 8);
			chunk = ChunkMap.get(key);
			if (chunk ==null){
				chunk = new Chunk(this, x,y);
				ChunkMap.put(key,chunk);
			}

		}
		return chunk;

	}




  public static int createVertex(float[] vertexBuffer, int i, Vector2 position, Vector3 normal, Vector2 uv){
    return createVertex(vertexBuffer, i, position.x, position.y, 0f, normal.x,normal.y,normal.z,uv.x,uv.y);
  }

  public static int createVertex(float[] vertexBuffer, int i, float x, float y, float z, Vector3 normal, Vector2 uv){
    return createVertex(vertexBuffer, i, x,y,z,normal.x,normal.y,normal.z,uv.x,uv.y);
  }

  public static int createVertex(float[] vertexBuffer, int i, float x, float y, float z, float nx, float ny, float nz, Vector2 uv){
    return createVertex(vertexBuffer, i, x,y,z,nx,ny,nz,uv.x,uv.y);
  }

  protected static int createVertex(float[] vertexBuffer, int i, float x, float y, float z, float nx, float ny, float nz, float u, float v){
    vertexBuffer[i++]  = x;
    vertexBuffer[i++]  = y;
    vertexBuffer[i++]  = z;
    //normal
    vertexBuffer[i++] = nx;
    vertexBuffer[i++] = ny;
    vertexBuffer[i++] = nz;
    //UV
    vertexBuffer[i++] = u;
    vertexBuffer[i++] = v;
    return i;
  }

	final int[] indicies1 = {0,1,2,0,2,3};

	final int[] indicies2 = {0,1,3,1,2,3};
	final static Vector3[] vects = { new Vector3(), new Vector3(), new Vector3(), new Vector3()};
	final static Vector3 u = new Vector3();
	final static Vector3 v = new Vector3();


	protected void addTriangle(Vector3 p1, Vector3 p2, Vector3 p3, float[] verts, short[] indicies){


	}


    ModelBuilder modelBuilder = new ModelBuilder();
    modelBuilder.begin();

    float[] vertexBuffer = new float[squares * 4 * 8];
    short[] edgeBuffer = new short[squares * 6];

    int pos = 0;
    int edgePos = 0;
    int vertOffset = 0;
		int depthWidth = def.width + 1;
    for(int y = 0; y < def.height; y++){
      for (int x = 0; x < def.width; x++) {

				int[] depths = {
						def.depths[(y * depthWidth) + x],
						def.depths[(y * depthWidth) + x  + 1],
						def.depths[((y + 1) * depthWidth) + x + 1],
						def.depths[((y + 1) * depthWidth) + x] };

				vects[0].set(x * Constants.TileSize, y * Constants.TileSize, depths[0] * Constants.LayerDepth);
				vects[1].set((x + 1) * Constants.TileSize, y * Constants.TileSize, depths[1] * Constants.LayerDepth);
				vects[2].set((x + 1) * Constants.TileSize, (y+ 1) * Constants.TileSize, depths[2] * Constants.LayerDepth);
				vects[3].set(x * Constants.TileSize, (y + 1) * Constants.TileSize, depths[3] * Constants.LayerDepth);

				int[] indicies;
				if( Math.abs( depths[0] - depths[2]) -   Math.abs( depths[1] - depths[3]) > 0){
					//triangle 0 1 2, 0 2 3
					indicies = indicies1;
				}
				else{
					indicies = indicies2;
					// 1 2 3, 1 3
				}

        //top left
        pos= createVertex(vertexBuffer, pos, x * Constants.TileSize, y * Constants.TileSize, def.depths[(y * depthWidth) + x] * Constants.LayerDepth, Vector3.Z.x,Vector3.Z.y,Vector3.Z.z, 0f,0f );
        //top right
        pos= createVertex(vertexBuffer, pos, (x + 1) * Constants.TileSize, y * Constants.TileSize, def.depths[(y * depthWidth) + x + 1] * Constants.LayerDepth, Vector3.Z.x,Vector3.Z.y,Vector3.Z.z, 1f,0f );
				//bottom right
				pos= createVertex(vertexBuffer, pos, (x + 1) * Constants.TileSize, (y + 1)  * Constants.TileSize, def.depths[((y+1) * depthWidth) + x + 1] * Constants.LayerDepth, Vector3.Z.x,Vector3.Z.y,Vector3.Z.z, 1f,1f );

				//bottom left
        pos= createVertex(vertexBuffer, pos, x * Constants.TileSize, (y + 1)  * Constants.TileSize, def.depths[((y+1) *depthWidth) + x] * Constants.LayerDepth, Vector3.Z.x,Vector3.Z.y,Vector3.Z.z, 0f,1f );

        edgeBuffer[edgePos++] = (short) (vertOffset);
        edgeBuffer[edgePos++] = (short) (vertOffset + 1);
        edgeBuffer[edgePos++] = (short) (vertOffset + 2);
        edgeBuffer[edgePos++] = (short) (vertOffset);
        edgeBuffer[edgePos++] = (short) (vertOffset + 2);
        edgeBuffer[edgePos++] = (short) (vertOffset + 3);

        vertOffset += 4;
      }
    }

    Material material = new Material(ColorAttribute.createDiffuse(Color.GREEN));

    MeshPartBuilder meshPartBuilder = modelBuilder.part("map", GL20.GL_TRIANGLES, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates,material );
    meshPartBuilder.addMesh(vertexBuffer, edgeBuffer);
		model = modelBuilder.end();
		modelInst = new ModelInstance(model);
  }


	ModelInstance modelInst;


  public void draw(ModelBatch modelBatch) {

		modelBatch.render(modelInst, env);

  }

  public void update() {
		modelInst.transform.rotate(Vector3.Z, 1f);
  }

  @Override
  public void dispose() {

  }
}

package net.ciderpunk.traingame.map;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by Matthew on 02/05/2016.
 */
public class MapDef{
  public int width;
  public int height;
  protected String tiles;
  public int[] depths;
  public String getTileData(){
    return tiles.replace(" ","").replace("\n","").replace("\r", "").replace("\t", "");
  }
}

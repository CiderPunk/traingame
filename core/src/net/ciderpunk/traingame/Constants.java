package net.ciderpunk.traingame;

/**
 * Created by Matthew on 07/05/2016.
 */
public class Constants {
  public static final float TileSize = 32f;
  public static final float LayerDepth = 16f;

  public static int ChunkSize = 16;

}

package net.ciderpunk.traingame;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import net.ciderpunk.traingame.map.Map;


public class TrainGame extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;

Viewport view;
	private Camera cam;
	private ModelBatch modelBatch;


	Map currentMap;

	@Override
	public void dispose() {
		super.dispose();
		modelBatch.dispose();



	}

	@Override
	public void create () {
		Gdx.app.log("game", "started");

		view = new FitViewport(800,600);

		cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam.position.set(0f,0f,400f);
		cam.lookAt(0,0,0);
		cam.near = 1f;
		cam.far = 2000f;
		cam.update();


		modelBatch = new ModelBatch();



    try {
      currentMap = new Map(new FileHandle("mapdefs/map1.json"));
    } catch (Exception e) {
			e.printStackTrace();
		}
	}



	@Override
	public void render () {
			currentMap.update();

		Gdx.gl.glClearColor(0.1f, 0.1f, 0.3f, 1);
			Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		modelBatch.begin(cam);
		currentMap.draw(modelBatch);
modelBatch.end();




	}
}
